<?php
/**
 * Created by PhpStorm.
 * User: zhengmingwei
 * Date: 2020/1/7
 * Time: 9:05 下午
 */

namespace Harvey\Toolsbox\library\Hashids;


class HashidsHelper
{
    private static $hashids;
    const SALT = 'harvey';

    /**
     * 单列模型实例化
     * @param $salt
     * @param $hashLength
     * @return Hashids
     */
    public static function getInstanceHashids($salt, $hashLength)
    {
        if (!self::$hashids instanceof Hashids) {
            self::$hashids =  new Hashids($salt, $hashLength);
        }
        return self::$hashids;
    }

    public static function encodeHex($str, $hashLength = 5)
    {
        $salt = self::SALT;

        $hashids = self::getInstanceHashids($salt, $hashLength);

        return $hashids->encodeHex($str);
    }

    public static function decodeHex($str, $hashLength = 5)
    {
        $salt = self::SALT;

        $hashids = self::getInstanceHashids($salt, $hashLength);

        return $hashids->decodeHex($str);
    }

}
